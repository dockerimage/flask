#!/bin/bash
crontab /etc/crontab
cron

export FLASK _APP=$FLASK_APP
if [ "$DEVELOPMENT_MODE" != "no" ]; then
    export FLASK_ENV=development 
fi
cd /home/flask
flask run --host=0.0.0.0 --port=80 --with-threads
