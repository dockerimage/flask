FROM ubuntu:20.04
LABEL description="Flask"

ENV TZ=Asia/Jakarta

RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt install -y tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt-get install -y unzip curl vim python3-pip
RUN pip3 install flask flask_cors requests httplib2 mysql-connector-python netaddr paramiko
RUN pip3 install kubernetes pyyaml
RUN apt install -y apt-transport-https ca-certificates
RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
RUN apt update -y
RUN apt install -y kubectl

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY ./home/flask/ /home/flask/

ENV APP_ENV '{}'
ENV DEVELOPMENT_MODE 'yes'
ENV FLASK_APP 'main'
ENV LOG_LEVEL 'INFO'

EXPOSE 80
VOLUME ["/home/flask", "/ext-dir/read", "/ext-dir/write", "/ext-dir/log_app"]
ENTRYPOINT ["./entrypoint.sh"]