from flask import Flask
from flask import request
from flask import Response

import os, sys, configparser, json
import logging
from logging.handlers import TimedRotatingFileHandler
import pprint

app = Flask(__name__)
currentDir = os.path.dirname(os.path.realpath(sys.argv[0]))
APP_ENV = json.loads(os.getenv('APP_ENV'))

if(os.getenv('LOG_LEVEL') == 'CRTITICAL'):
    logLevel = 50
elif(os.getenv('LOG_LEVEL') == 'ERROR'):
    logLevel = 40
elif(os.getenv('LOG_LEVEL') == 'WARNING'):
    logLevel = 30
elif(os.getenv('LOG_LEVEL') == 'INFO'):
    logLevel = 20
elif(os.getenv('LOG_LEVEL') == 'DEBUG'):
    logLevel = 10
elif(os.getenv('LOG_LEVEL') == 'NOTSET'):
    logLevel = 0
else:
    logLevel = 20
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logLevel)
logging.getLogger().setLevel(logLevel)
logObj = logging
app.logger.addHandler(logObj)

@app.route('/', methods=['GET', 'POST'])
def index():
    return 'Flask App'
